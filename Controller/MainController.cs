﻿using League_of_Legends_Project.API;
using League_of_Legends_Project.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RiotSharp;
using System.Windows;

namespace League_of_Legends_Project.Controller
{
   public class MainController
    {
        //public bool getsummoner(string sumname/*,ref string id*/)
        //{


        //    Summoner_V4 summoner_V4 = new Summoner_V4(Constants.Region);
        //    var summoner = summoner_V4.GetSummonerByNameAsync(sumname).Result;
        //    Constants.Summoner = summoner;

        //    // if(summoner!=null) id = summoner.Id;
        //    return summoner != null;
        //    return false;

        //}
        public bool GetsummonerRiotSharp(string sumname/*,ref string id*/)
        {
            Summoner_V4 summoner_V4 = new Summoner_V4(Constants.Region);
            var summoner = summoner_V4.GetSummonerBynameRiotsharpAsync(sumname);
            Constants.Summoner = summoner.Result;

            // if(summoner!=null) id = summoner.Id;
            return summoner != null;

        }

    }
}
