﻿using League_of_Legends_Project.API;
using League_of_Legends_Project.Model;
using League_of_Legends_Project.Utils;
using League_of_Legends_Project.View.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using RiotNet.Models;
//using RiotSharp;
using RiotSharp.Endpoints.SummonerEndpoint;
using RiotSharp.Endpoints.LeagueEndpoint;

namespace League_of_Legends_Project.Controller
{
    class ProfileController
    {
        public object GetContext() 
        {
          Summoner summoner = Constants.Summoner;
            RiotSharp.Endpoints.LeagueEndpoint.LeagueEntry position = GetPostion(summoner);
            var viewprofile= new ViewModelProfile(summoner.Name, summoner.ProfileIconId, summoner.Level, position.Tier,
                position.Rank, position.Wins, position.Losses);
            return viewprofile;
        }

        private RiotSharp.Endpoints.LeagueEndpoint.LeagueEntry GetPostion(Summoner summoner)
        {
            //RiotNet.Models.LeaguePosition pos;
            RiotSharp.Endpoints.LeagueEndpoint.LeagueEntry pos;
          

            try
            {
                League_V4 league_V4 = new League_V4(Constants.Region);
                var position = league_V4.GetPosRiotsharpAsync(summoner.Id).Result.Where(p => p.QueueType.Equals(RiotSharp.Misc.Queue.RankedSolo5x5)).FirstOrDefault();
                pos = position;
               

            }
            catch (RiotSharp.RiotSharpException e)
            {
                string m = e.Message;
                pos = null;
            }
            return pos;
       
            
        }
    }
}
