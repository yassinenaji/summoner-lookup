﻿using League_of_Legends_Project.Utils;
using RiotSharp.Endpoints.StaticDataEndpoint;
using RiotSharp.Endpoints.ChampionEndpoint;
using RiotSharp.Endpoints.StaticDataEndpoint;
using RiotSharp.Endpoints.LeagueEndpoint;
using RiotSharp.Endpoints.SummonerEndpoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RiotSharp;
using RiotSharp.Endpoints.StaticDataEndpoint.Champion;
using RiotSharp.Misc;
using League_of_Legends_Project.View.ViewModel;
using League_of_Legends_Project.API;
using System.Text.RegularExpressions;

namespace League_of_Legends_Project.Controller
{
    class ChampionsController
    {

        public ChampionsController() { }



        //public object GetContext()
        //{


        //    List<ViewModelChampions> viewprofile = new List<ViewModelChampions>();
        //    foreach (var item in viewprofile)
        //    {
        //        item
        //    }
        //    return viewprofile;
        //}

        public List<ViewModelChampions> GetAllChampionsContext()
        {

            var cham = CHAMPION_V3.GetChampionsByKey().ToList();
            List < ViewModelChampions > viewmodelchampions = new List<ViewModelChampions>();
            List<string> Items = new List<string>();
            

            foreach (ChampionStatic champion in cham)
            {
                string tags = " ";
                foreach (RiotSharp.Endpoints.StaticDataEndpoint.Champion.Enums.TagStatic tag in champion.Tags)
                {
                    tags +=" "+tag.ToString();
                }
                List<string> items = new List<string>();
                var currchamp = cham.Where(p => p.Name.Equals(champion.Name)).FirstOrDefault();
                foreach (var recItems in currchamp.RecommendedItems)
                {


                    RiotSharp.Endpoints.StaticDataEndpoint.Item.ItemStatic item = new RiotSharp.Endpoints.StaticDataEndpoint.Item.ItemStatic();
                    items.Add(recItems.Title);
                }
                viewmodelchampions.Add(new ViewModelChampions(champion.Name , tags, champion.Image.Full,champion.Lore,items));
                tags = " ";



            }
            return viewmodelchampions;

        }
        public RiotSharp.Endpoints.StaticDataEndpoint.Champion.ChampionStatic GetChampionsByName(string name)
        {

            var champ =CHAMPION_V3.GetChampionsByKey().Where(p => p.Name.Equals(name)).FirstOrDefault();
            
         
            return champ;

        }
    }
}
