﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using League_of_Legends_Project.Controller;

namespace League_of_Legends_Project.View
{
    /// <summary>
    /// Logique d'interaction pour SummonerProfil.xaml
    /// </summary>
    public partial class SummonerProfil : Window
    {
        ProfileController profileController;
        public SummonerProfil()
        {
            profileController = new ProfileController();
            InitializeComponent();
            this.DataContext = profileController.GetContext();
        }
    }
}
