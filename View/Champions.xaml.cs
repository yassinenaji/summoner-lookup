﻿using League_of_Legends_Project.View.ViewModel;
using System.Collections.Generic;
using System.Data;
using System.Windows;
using System.Windows.Controls;

namespace League_of_Legends_Project.View
{
    /// <summary>
    /// Logique d'interaction pour Champions.xaml
    /// </summary>
    public partial class Champions : Window
    {
       Controller.ChampionsController championsControlle=new Controller.ChampionsController();
       List<ViewModelChampions> listch;
        public Champions()
        {
            InitializeComponent();
             listch = championsControlle.GetAllChampionsContext();
            
            Imported.ItemsSource = null;
            Imported.ItemsSource = listch;
            this.DataContext = listch;
        }

        private void Imported_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            //var ch = listch.Where(ls => ls.ChampionName.Equals().FirstOrDefault();


            
            
        
        }

        private void Imported_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid grid = (DataGrid)sender;
            //DataRowView row_sel = grid.SelectedItem as DataRowView;
            ViewModelChampions row_sel = grid.SelectedItem as ViewModelChampions;
            //if (row_sel!=null)

            var i = "";
            foreach (var item in row_sel.ItemsTitle)
            {
                i += item;
            }

         MessageBox.Show(row_sel.Lore+"\n"+i);
           
        }
    }
}
