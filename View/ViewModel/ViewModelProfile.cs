﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RiotSharp;

namespace League_of_Legends_Project.View.ViewModel
{
    class ViewModelProfile
    {

        public string SummonerName { get; private set; }
        public string Icon { get; private set; }
        public long Level { get; private set; }
        public string Tier { get; private set; }
        public string Rank { get; private set; }
        public int Wins { get; private set; }
        public int Losses { get; private set; }
        public string Emblem { get; private set; }

        public ViewModelProfile(string n, int i, long l, string t, string r, int w,int lo) 
        {
            this.SummonerName = n;
            this.Icon = "http://ddragon.leagueoflegends.com/cdn/10.10.3216176/img/profileicon/" + i + ".png";
            this.Level = l;
            this.Tier = t;
            this.Rank = r;
            this.Wins = w;
            this.Losses = lo;
            //this.Emblem = "Assets/emblems/Emblem_"+this.Tier+".png";
            this.Emblem = "/League_of_Legends_Project;component/Assets/emblems/Emblem_" + this.Tier+".png";
        }

        
    }
}
