﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace League_of_Legends_Project.View.ViewModel
{
    class ViewModelChampions
    {
        public string ChampionName { get; private set; }
        public string Icon { get; private set; }
        public string Role { get; private set; }

        public string Lore { get; private set; }

        public List<string> ItemsTitle { get; private set; }






        public ViewModelChampions(string championName,string Role,string image,string lore, List<string> items) 
        {
           
            this.ChampionName =championName;
            this.Role = Role;
            this.Lore = lore;
             this.Icon = "http://ddragon.leagueoflegends.com/cdn/10.10.3216176/img/champion/"+image;
            this.ItemsTitle = items;
           

        }
    }
}
