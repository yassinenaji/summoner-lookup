﻿using League_of_Legends_Project.API;
using League_of_Legends_Project.Controller;
using League_of_Legends_Project.View;
using League_of_Legends_Project.View.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace League_of_Legends_Project
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainController controller;
        ViewModelMain viewModel;
        ChampionsController championsController=new ChampionsController();
       
        public MainWindow()
        {
           viewModel = new ViewModelMain();
           controller = new MainController();
            
            InitializeComponent();
            this.DataContext = viewModel;
        }
        private void ButtonSearch_Click(object sender, RoutedEventArgs e)
        {
            //string id="";
            if (string.IsNullOrEmpty(viewModel.Region))
                 return;
            if (string.IsNullOrEmpty(viewModel.SummonerName))
              return;
            //RiotSharp.Misc.Region region1;
            //Enum.TryParse(viewModel.Region, out region1);
            //var reg = region1.ToString();
            if (controller.GetsummonerRiotSharp(viewModel.SummonerName))
            {
             
                MessageBox.Show("found");
                SummonerProfil smp = new SummonerProfil();
                smp.Show();
                this.Close();

            }
            else MessageBox.Show("not found") ;

          




        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            //string allnames = "default";
            //var champ = championsController.GetChampionsByName("Fiora");
            //allnames = "Champion name  :" + champ.Name + "Passive :"+champ.Passive.Name;
            //MessageBox.Show(allnames.ToString());
          


        }
    }
}
