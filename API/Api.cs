﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using RiotNet;
using RiotNet.Models;
using RiotSharp;

namespace League_of_Legends_Project.API
{
   public class Api
    {
        private string Key { get; set; }
        private string Region { get; set; }
        public static int maxl; 

        public Api(string region) 
        {
            this.Region = region;
            this.Key = GetKey("API/Key.txt");
        }

        protected HttpResponseMessage GET(string URL) 
        {
            using (HttpClient client=new HttpClient())
            {
                var result = client.GetAsync(URL);
                result.Wait();
                return result.Result;
               
            }
            


        }
        protected string GetURI(string path) 
        {
           // return "https://" + Region + ".api.riotgames.com/lol/" + path + "?api_key=" + Key;
            return "https://" + Region + ".api.riotgames.com/lol/" + path + "?api_key=" + Key;


        }

        public static string GetKey(string path) 
        {
            StreamReader sr = new StreamReader(path);

            return sr.ReadToEnd();
        }
     
     public static   IRiotClient client() 
        {

            RiotClient.DefaultSettings = () => new RiotClientSettings
            {
                ApiKey = GetKey("API/Key.txt") // Replace this with your API key, of course.
            };
            IRiotClient client = new RiotClient();
          
            return client;
        }
        public static RiotSharp.RiotApi  RiotSharpClient()
        {

            var api = RiotApi.GetInstance(GetKey("API/Key.txt"), 20, 100);
            

            return api;
        }


    }
}
