﻿using League_of_Legends_Project.Model;
using League_of_Legends_Project.Utils;
using Newtonsoft.Json;
//using RiotNet;
//using RiotNet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RiotSharp;

using RiotSharp.Endpoints.LeagueEndpoint;
using RiotNet;

namespace League_of_Legends_Project.API
{
    class League_V4 : Api
    {
        public League_V4(string region) : base(region)
        {
        }
      public List<PositionDTO> GetPositions(string Summonerid)
        {
            string path = "league/v4/entries/by-summoner/" + Summonerid;

            var response = GET(GetURI(path));
            string content = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<List<PositionDTO>>(content);
            }
            else
            {
                return null;
            }


        }

            public async Task<List<RiotNet.Models.LeagueEntry>> getPosAsync(string Summonerid)
            {
            

           
            var position = await client().GetLeagueEntriesBySummonerIdAsync(Summonerid, Constants.Region).ConfigureAwait(false);
            if (position != null)
                return position;
            else
                return null;
        }

        public async Task<List<RiotSharp.Endpoints.LeagueEndpoint.LeagueEntry>> GetPosRiotsharpAsync(string Summonerid) 
        {

            try
            {
              
                var position = await RiotSharpClient().League.GetLeagueEntriesBySummonerAsync(Constants.Summoner.Region, Summonerid).ConfigureAwait(false);






                return position;
            }
            catch (RiotSharpException e)
            {
                string mess=e.Message;
                return null;
            }
           
        }
    }
}
