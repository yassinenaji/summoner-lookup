﻿using League_of_Legends_Project.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using RiotNet.Models;
using RiotNet;
using League_of_Legends_Project.Utils;
using RiotSharp.Endpoints.SummonerEndpoint;
using RiotSharp;

namespace League_of_Legends_Project.API
{
   public class Summoner_V4 : Api
    {
        public Summoner_V4(string region) : base(region)
        {

        }


    //public async Task<Summoner> GetSummonerByNameAsync(string SummonerName) 
    //{

    //    /*string path = "summoner/v4/summoners/by-name/" + SummonerName;

    //    var response = GET(GetURI(path));
    //    string content = response.Content.ReadAsStringAsync().Result;

    //    if (response.StatusCode == System.Net.HttpStatusCode.OK)
    //    {
    //        return JsonConvert.DeserializeObject<Summoner>(content);
    //    }
    //    else
    //    {
    //        return null;
    //    }*/

    //    IRiotClient client = new RiotClient(new RiotClientSettings
    //    {
    //        ApiKey = GetKey("API/Key.txt")

    //    });


    //    var Summoner = await client.GetSummonerBySummonerNameAsync(SummonerName, Constants.Region).ConfigureAwait(false);

    //    return Summoner;

    //}

    public async Task<Summoner> GetSummonerBynameRiotsharpAsync(string SummonerName) 
        {
            try
            {
                var reg = (RiotSharp.Misc.Region)Enum.Parse(typeof(RiotSharp.Misc.Region), Constants.Region,true);


                var summoner =  await RiotSharpClient().Summoner.GetSummonerByNameAsync(reg,SummonerName).ConfigureAwait(false);

                Constants.Summoner = summoner;
               

                // if(summoner!=null) id = summoner.Id;
                return summoner;
            }
            catch (RiotSharpException e)
            {
                string mess = e.Message;
                return null;
            }


        }

        public async Task<RiotNet.Models.Summoner> GetSummonerBynameRiotNetAsync(string Summonerid)
        {

         


            var summoner = await client().GetSummonerByAccountIdAsync(Summonerid, Constants.Region).ConfigureAwait(false);
            if (summoner != null)
                return summoner;
            else
                return null;
        }
    }
}
