﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace League_of_Legends_Project.Model
{
    class MiniSeriesDTO
    {
        [JsonProperty("target")]
        public long Target { get; set; }

        [JsonProperty("wins")]
        public long Wins { get; set; }

        [JsonProperty("losses")]
        public long Losses { get; set; }

        [JsonProperty("progress")]
        public string Progress { get; set; }
    }
}
